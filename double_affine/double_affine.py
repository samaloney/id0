from collections import defaultdict 
from hashlib import md5
from math import gcd

def egcd(a, b):
    x,y, u,v = 0,1, 1,0
    while a != 0:
        q, r = b//a, b%a
        m, n = x-u*q, y-v*q
        b,a, x,y, u,v = a,r, u,v, m,n
    gcd = b
    return gcd, x, y


def modinv(a, m):
    gcd, x, y = egcd(a, m)
    if gcd != 1:
        return None  # modular inverse does not exist
    else:
        return x % m


def affine_enc(a, b, alphabet, msg):
    text = []
    m = len(alphabet)
    for i in range(0, len(msg), 2):
        block = msg[i:i+2]
        code = alphabet.index(block[0]) * m + alphabet.index(block[1]) 
        enc = (a * code + b) % m**2
        c1, c2 = divmod(enc, m)
        text.append(alphabet[c1] + alphabet[c2])
    return "".join(text)


def affine_dec(a, b, alphabet, msg):
    text = []
    m = len(alphabet)
    ainv = modinv(a, m**2)
    for i in range(0, len(msg), 2):
        block = msg[i:i+2]
        code = alphabet.index(block[0]) * m + alphabet.index(block[1]) 
        dec = ainv* (code - b) % m**2
        c1, c2 = divmod(dec, m)
        text.append(alphabet[c1] + alphabet[c2])
    return "".join(text)   


def bigrams(msg):
    d = defaultdict(int)
    for i in range(0, len(msg), 2):
        cur = msg[i:i+2]
        d[cur] += 1
    return d


codex = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ ,.'

test_text = 'HELLO WORLD.'
res_enc = affine_enc(2, 3, codex, test_text)
res_dec = affine_dec(2, 3, codex, res_enc)
if res_dec == test_text:
    print('En/De-cryption test passed')

with open('message', 'r') as file:
    msg = file.read()

with open('bigrams.txt', 'r') as file:
    wf = defaultdict(float)
    for line in file.readlines():
        letter, freq = line.strip().split(' ')
        wf[letter] = float(freq.strip()) 

tw = sum(wf.values())
m_len = len(msg)
wp = {k:(v/tw)*(m_len/2) for k,v in wf.items()}

chis = []
ab = []
for a in range(1, len(codex)**2):
    if gcd(a, len(codex) **2) == 1:
        for b in range(1, len(codex)**2):
            poss = affine_dec(a, b, codex, msg.strip())
            cf = bigrams(poss) 
            chi2 = sum([ ((cf[key] - wp[key])**2)/wp[key] for key in wp])
            ab.append([a,b])  
            chis.append(chi2)

best_ind = chis.index(min(chis))
best_ab = ab[best_ind]

res = affine_dec(best_ab[0], best_ab[1], codex, msg.strip())
resmd5 = md5(res.encode())
print(resmd5.hexdigest())
