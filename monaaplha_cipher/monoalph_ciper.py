from math import log10
from string import ascii_uppercase as alpha
from random import shuffle, randint
from hashlib import md5


cipher_text = "EANOEHHNFJLFXDGFANOYDJINTNOEDJXFOSBFESDOLGDWWSNUEDJNQCSJNUFEFFOUIDTTCOSIFESDOLNJKSINLEDNOXSONNJEZNSJMJDUCIELEDXCFJFOENNGFANOYDJINTNOEFIINLLEDFGGUFEFFYENJGNOXEZHUNWFENFOUKSXDJDCLMJNUSIESDOLDYNOYDJINTNOEIZFOONGLXDSOXUFJREZNLNFEENTMELEDJNXCGFENEZNNTNJXSOXSOENJONEANJNFWFOUDONUSOEZNSOENJKNOSOXHNFJLSOODKFESDODOEZNSOENJONEYGDCJSLZNUFOUGFANOYDJINTNOEFXNOISNLYDCOUONAFOUTDJNNYYNIESKNTNFOLDYFIINLLSOXKFLEGHGFJXNJQCFOESESNLDYUFEFEDUFHANFJNFXFSOZNFJSOXIFGGLYDJJNXCGFESDOEDTFOUFENEZNMJDKSLSDODYNPINMESDOFGFIINLLTNIZFOSLTLSOEZSLJNMDJEFXJDCMDYIDTMCENJLISNOESLELFOULNICJSEHNPMNJELTFOHDYAZDTMFJESISMFENUSOFLECUHDYEZNLNLFTNEDMSILZFLIDOKNONUEDNPMGDJNEZNGSRNGHNYYNIELDYSTMDLSOXNPEJFDJUSOFJHFIINLLTFOUFENLANZFKNYDCOUEZFEEZNUFTFXNEZFEIDCGUWNIFCLNUWHGFANOYDJINTNOENPINMESDOFGFIINLLJNQCSJNTNOELADCGUWNNKNOXJNFENJEDUFHEZFOSEADCGUZFKNWNNOHNFJLFXDSOEZNAFRNDYEZNXJDASOXNIDODTSIFOULDISFGIDLEDYEZNYCOUFTNOEFGSOLNICJSEHDYEDUFHLSOENJONENOKSJDOTNOEFOHMJDMDLFGLEZFEFGENJEZNLNICJSEHUHOFTSILDOGSONLZDCGUWNFMMJDFIZNUASEZIFCESDONPINMESDOFGFIINLLADCGUYDJINSOENJONELHLENTUNKNGDMNJLEDJNKNJLNYDJAFJULNIJNIHUNLSXOMJFIESINLEZFELNNREDTSOSTSBNEZNSTMFIEDOCLNJMJSKFIHAZNOLHLENTLFJNWJNFIZNUEZNIDTMGNPSEHDYEDUFHLSOENJONENOKSJDOTNOEASEZTSGGSDOLDYFMMLFOUXGDWFGGHIDOONIENULNJKSINLTNFOLEZFEONAGFANOYDJINTNOEJNQCSJNTNOELFJNGSRNGHEDSOEJDUCINCOFOESISMFENUZFJUEDUNENIELNICJSEHYGFALWNHDOUEZNLNFOUDEZNJENIZOSIFGKCGONJFWSGSESNLEZNMJDLMNIEDYXGDWFGGHUNMGDHNUNPINMESDOFGFIINLLLHLENTLJFSLNLUSYYSICGEMJDWGNTLFWDCEZDALCIZFONOKSJDOTNOEADCGUWNXDKNJONUFOUZDAEDNOLCJNEZFELCIZLHLENTLADCGUJNLMNIEZCTFOJSXZELFOUEZNJCGNDYGFA"

def encrypt(message, key):
    out = []
    for char in message:
        ind = alpha.index(char)
        out.append(key[ind])
    return "".join(out)


def decrypt(message, key):
    out = []
    for char in message:
        ind = key.index(char)
        out.append(alpha[ind])
    return "".join(out)


def bigrams(msg, size=1):
    d = defaultdict(int)
    for i in range(0, len(msg) - size):
        cur = msg[i:i+size]
        d[cur] += 1
    return d


tkey = 'UHMZGIJRSNWDPBFLQAVOCETYKX'
tcipher = 'RGDDFTFADZ'
tclear = 'HELLOWORLD'
if tclear == decrypt(tcipher, tkey):
    print("Dectypt test passed")

with open('english_quadgrams.txt', 'r') as file:
    ngrams = dict()
    for line in file.readlines():
        ngram, count = line.split(' ')
        ngrams[ngram] = float(count)

    total = sum(ngrams.values())
    for k, v in ngrams.items():
        ngrams[k] = log10(v/total)
    minp = log10(0.01/total)


def score(text, ngrams):
    score = 0
    size = len(next (iter (ngrams.keys())))
    for i in range(len(text) - size):
        if text[i:i+size] in ngrams: score += ngrams[text[i:i+size]]
        else: score += minp
    return score

max_score = -99e9
max_key = list(alpha)
parent_score, parent_key = max_score, max_key[:]
i = 0
while i < 10:
    shuffle(parent_key)
    plain = decrypt(cipher_text, parent_key)
    parent_score = score(plain, ngrams)
    count = 0
    while count < 1000:
        a = randint(0,25)
        b = randint(0,25)
        child_key = parent_key[:]
        child_key[a], child_key[b] = child_key[b], child_key[a]
        plain = decrypt(cipher_text, child_key)
        child_score = score(plain, ngrams)
        if child_score > parent_score:
            parent_score = child_score
            parent_key = child_key[:]
            count = 0
        count += 1
    if parent_score > max_score:
        max_score = parent_score
        max_key = parent_key[:]
        print(max_score, max_key)
    i += 1

plain = decrypt(cipher_text, max_key)
print(md5(plain).hexdigest())
