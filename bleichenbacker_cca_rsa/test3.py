from binascii import hexlify, unhexlify
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5
from requests import Session

session = Session()

with open('key', 'r') as file:
    key_str = file.read()

key = RSA.importKey(key_str)

with open('cipher', 'rb') as file:
    ctext = file.read()

cipher_ = PKCS1_v1_5.new(key)

n = 0x4c81390477e071a7a9afd85eeb93f3596cf69fb8e7fadf422f22c68891586611af5e74aa8b4df9a585486898f632ae63
e = 0x3
c0 = 0x1cb75d15d80c8bd7572281de5da592a428db429870b4a654b8722f98acc220b6701f6c0b7313fb9ef4ca15a87d9273bb

server ='https://id0-rsa.pub/problem/pkcs15-oracle/'

#c0 = int(hexlify(ctext), base=16)
#n = key.key.n
#e = key.key.e
#m = pow(c0, key.key.d, n)
k = (n.bit_length() + 7)//8
B = 2 ** (8*(k-2))

#def is_pkcs1_formatted(cipher):
#    ret = cipher_.decrypt(cipher.to_bytes(32, 'big'), None)       
#    if ret == None:
#        return False
#    else:
#        return True

def is_pkcs1_formatted(cipher):
    ret = session.get(server+str(cipher))      
    if ret.text == '0':
        return False
    else:
        return True

def myfloor(a, b):
    res = (a // b);
    return res;


def myceil(a,b):
    res = (a // b);
    if (a % b):
        res += 1;
    return res;
    

def bb98_2a():
    s = myceil(n, 3*B);
    cipher = c0 * ( s ** e ) % n
    while (not is_pkcs1_formatted(cipher)):
        s += 1;
        cipher = c0 * ( s ** e ) % n
        #print(s, hex(cipher))
    print("Step 2a returning ", s)
    return s;

def bb98_2b(prev_s):
    s = prev_s + 1;
    cipher = c0 * ( s ** e ) % n
    while (not is_pkcs1_formatted(cipher)):
        s += 1;
        cipher = c0 * ( s ** e ) % n
        #print(s, hex(cipher))
    print("Step 2b returning ", s)
    return s;


def bb98_2c(intervals, s_prev):
    assert(len(intervals) == 1);
    (a,b) = intervals[0];
    r = myceil(2*(b*s_prev - 2*B), n);
    s = myceil((2*B + r*n), b);
    while (True):
        cipher = c0 * (s ** e) % n
        if (is_pkcs1_formatted(cipher)):
            return (r,s);
        s = s+1;
        if (s > myfloor(3*B + r*n, a)):
            r += 1;
            s = myceil((2*B + r*n), b);
    
    
def bb98_3(prev_intervals, s):
    intervals = [];
    for (a,b) in prev_intervals:
        print("using %d, %d from prevoius interval" % (a,b))
        min_r = myceil(a*s - 3*B + 1, n);
        max_r = myfloor(b*s - 2*B, n);
        for r in range(min_r, max_r+1):
            aa = myceil(2*B + r*n, s);
            bb = myfloor(3*B - 1 + r*n, s);
            #print ("step3: aa, bb: ", aa, bb);
            lower_bound = max(aa, a);
            upper_bound = min(bb, b);
            #print("step3: lower, upper: ", lower_bound, upper_bound)
            if (lower_bound > upper_bound):
                continue;
            #print("Appending %d, %d" %(lower_bound, upper_bound))
            #if (lower_bound <= prob47_message and prob47_message <= upper_bound):
                #print("Answer in range");
            #else:
                #print("ANSWER OUT OF RANGE!!!!");
            bb98_append(intervals, lower_bound, upper_bound);
    print(intervals)
    return intervals;
            
def bb98_append(intervals, lower_bound, upper_bound):
    '''Appends the new interval to the existing set of intervals,
    reducing any overlaps, ie (2,7),(5,9) reduces to (2,9)
    and (2,7), (3,5) reduces to (2,7)'''
    for i in range(len(intervals)):
        # Given (l1, u1),(l2,u2) we care about everything except:
        # (u1 < l2) or (u2 < l1)
        if (intervals[i][1] < lower_bound):
            continue;
        if (upper_bound < intervals[i][0]):
            continue;
        # we must have overlap, thus replace with lower_bound, upper_bound with
        # min,max of (intervals[i], upper, lower)
        lower_bound = min(intervals[i][0], intervals[i][1], lower_bound);
        upper_bound = max(intervals[i][0], intervals[i][1], upper_bound);
        # remove the overlapping interval from the set
        #print("removing index %d" % i);
        intervals.remove(i);
        # add the new interval
        bb98_append(intervals, lower_bound, upper_bound);
        return;
    # if we reach here, then no overlap found -- just add interval
    intervals.append([lower_bound, upper_bound]);



if __name__ == "__main__":
    s = bb98_2a();
    prev_intervals = [[2*B, 3*B-1]];
    prev_intervals = bb98_3(prev_intervals, s);
    while (True):
        #print("intervals: " , prev_intervals);
        if (len(prev_intervals) == 1):
            (a,b) = prev_intervals[0];
            if (a == b):
                print("Message: " + hex(a));
                break;
            r, s = bb98_2c(prev_intervals, s);
        else:
            s = bb98_2b(prob47_key, s)
        prev_intervals = bb98_3(prev_intervals, s);
    
    print("Done")
