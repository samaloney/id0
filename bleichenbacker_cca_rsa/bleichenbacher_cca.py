from queue import Queue
from threading import Thread
from binascii import unhexlify, hexlify
from math import ceil, floor
import requests


def egcd(a, b):
    x,y, u,v = 0,1, 1,0
    while a != 0:
        q, r = b//a, b%a
        m, n = x-u*q, y-v*q
        b,a, x,y, u,v = a,r, u,v, m,n
    gcd = b
    return gcd, x, y


def modinv(a, m):
    gcd, x, y = egcd(a, m)
    if gcd != 1:
        return None  # modular inverse does not exist
    else:
        return x % m


N = '0x4c81390477e071a7a9afd85eeb93f3596cf69fb8e7fadf422f22c68891586611af5e74aa8b4df9a585486898f632ae63'
E = '0x3'
ciphertext = '0x1cb75d15d80c8bd7572281de5da592a428db429870b4a654b8722f98acc220b6701f6c0b7313fb9ef4ca15a87d9273bb'

server ='https://id0-rsa.pub/problem/pkcs15-oracle/'

n = int(N, base=16)
e = int(E, base=16)
c = int(ciphertext, base=16)
k = len(unhexlify(N[2:])) * 8
B = 2**(k-16)
B2 = 2*B
B3 = 3*B
#s_min = ceil(n/B3)
s_min = 8990
session = requests.Session()

a=B2
b=B3 - 1

for i in range(s_min, b):
    print(i)
    c_prime = (c * (i ** e)) % n
    res = session.get(server + str(c_prime))
    if res.text == '1':
        break

si = i
r = ceil((b*si - B2)*2/n)
i2c,nr = 0,1
found = False

while not found:
    for si in range(ceil((B2 + r * n)/b),floor((B3-1 + r * n)/a)+1):
        i2c += 1
        c_prime = (c * (si ** e)) % n
        res = session.get(server + str(c_prime))
        if res.text == '1':
            found = True
            si = i
            break
    
    if not found:
        r += 1
        nr += 1

    print("[*] Search done in %i iterations" % (i2c))
    print("    explored values of r:  %i" % nr)
    print("    s_%i:                    %i" % (si, si))
