from binascii import hexlify, unhexlify
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5
#from math import ceil, floor

def floor(a, b):
    res = (a // b);
    return res;
    
def ceil(a, b):
    res = (a // b);
    if (a % b):
        res += 1;
    return res;

ncalls = 0
def check_padding(s):
    global ncalls
    ncalls += 1
    return cipher.decrypt(s.to_bytes(32, 'big'), None)
    
with open('key', 'r') as file:
    key_str = file.read()

key = RSA.importKey(key_str)

with open('cipher2', 'rb') as file:
    ctext = file.read()

cipher = PKCS1_v1_5.new(key)

c = int(hexlify(ctext), base=16)
n = key.key.n
e = key.key.e
m = pow(c, key.key.d, n)
k = (len(hex(n)) - 2)//2
B = 2 ** (8*(k-2))
B2 = 2*B
B3 = 3*B
s_min = ceil(n, B3)

M = [[B2, B3 -1]]
i = 1
s = [1]

found = False
while not found:
    if i == 1:
        s1 = s_min
        while True:
            cp = c * (s1 ** e) % n
            res = check_padding(cp)

            if res != None:
                print('Found s1: ', s1)
                s.append(s1)
                break

            s1 += 1
    elif i > 1 and len(M[i-1]) > 4:
        print('Multi interval search')
    elif len(M[i-1]) == 2:
        print('Searching for s%i' % i)
        r = ceil(2 * ((M[i-1][1] * s[i-1] - B2)), n)
        while True:
            aa = ceil((B2 + r*n), M[i-1][1])
            bb = floor((B3 + r*n), M[i-1][0])
            for si in range(aa, bb, 1):
                cp = c * (si ** e) % n
                res = check_padding(cp)
                if res != None:
                    print('Found s%i %i' % (i, si))
                    s.append(si)
                    break

            if res != None:
                break
            else:
                r += 1

    print('Narrow solution set')
    rmin = ceil((M[i-1][0] * s[i] - B3 +1), n)
    rmax = floor((M[i-1][1] * s[i] - B2), n)
    if rmin == rmax:
        rs = [rmin]
    elif rmax > rmin:
        rs = list(range(rmin, rmax))
    
    print('rmin, rmax: ', rmin, rmax, rs)
    intervals=[]
    for r in rs:
        aa = ceil((B2 + r*n), s[i])
        bb = floor((B3 - 1 + r*n), s[i])
        if not (aa <= m and m <= bb):
             print("message not in range")
        newa = max(M[i-1][0], aa)
        newb = min(M[i-1][1], bb)
        intervals.append(newa)
        intervals.append(newb)

    print('New intervals: ', intervals)
    M.append(intervals)
    
    if len(intervals) == 2 and intervals[1] -  intervals[0] <= 5:
        print('Done')
        found = True
    else:
        i += 1

print(ncalls)
