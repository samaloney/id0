from binascii import unhexlify, hexlify
from math import ceil, floor
from threading import Thread
import requests
import queue
import threading


def egcd(a, b):
    x,y, u,v = 0,1, 1,0
    while a != 0:
        q, r = b//a, b%a
        m, n = x-u*q, y-v*q
        b,a, x,y, u,v = a,r, u,v, m,n
    gcd = b
    return gcd, x, y


def modinv(a, m):
    gcd, x, y = egcd(a, m)
    if gcd != 1:
        return None  # modular inverse does not exist
    else:
        return x % m


N = '0x4c81390477e071a7a9afd85eeb93f3596cf69fb8e7fadf422f22c68891586611af5e74aa8b4df9a585486898f632ae63'
E = '0x3'
ciphertext = '0x1cb75d15d80c8bd7572281de5da592a428db429870b4a654b8722f98acc220b6701f6c0b7313fb9ef4ca15a87d9273bb'

server ='https://id0-rsa.pub/problem/pkcs15-oracle/'

n = int(N, base=16)
e = int(E, base=16)
c = int(ciphertext, base=16)
k = len(unhexlify(N[2:])) * 8
B = 2**(k-16)
B2 = 2*B
B3 = 3*B
#s_min = ceil(n/B3)
s_min = 8990

def chunker(seq, size):
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))

session = requests.Session()


def worker(wq):
    global res
    while True:
        item = wq.get()
        if item is None:
            break
        cp = c * (item ** e) % n
        resp = session.get(server+str(cp))
        if resp.text == '1':
            res.append(item)

        wq.task_done()


n_workers = 1000
res = []


def query_oracle(smin, smax):
    q = queue.Queue()
    threads = []
    for i in range(n_workers):
        t = threading.Thread(target=worker, args=(q,))
        t.start()
        threads.append(t)

    start = smin
    end = start + 10000
    if end > smax: end = smax
    while not end > smax:
        [q.put(si) for si in range(start, end)]
        q.join()
        if len(res) > 0:
            for i in range(n_workers):
                q.put(None)
            for t in threads:
                t.join()
            return
        start = end
        end += 10000
        if end > smax: end = smax
        if end == smax: break
    
    for i in range(n_workers):
        q.put(None)
    for t in threads:
        t.join()

found = False
i = 0
s = [1]
M = [[B2, B3-1]]
while not found:
    if i == 0:
        i = 1
        res = []
        print("Initial Search")
        s_min = 8990 #ceil(n/B3)
        query_oracle(s_min, B3 - 1)
        s.append(res[0])
        print("Found s1: ", s[i])
    elif i > 1 and len(M[i-1]) > 2:
        print("Multi interval search shouldnt get here")
    elif len(M[i-1]) == 2:
        res = []
        print("Search over interval for s%s" % i)
        r = ceil(2*((M[i-1][1] * s[i-1] - B2)/n))
        while True:
            aa = ceil((B2 + r*n)/M[i-1][1])
            bb = floor((B3 + r*n)/M[i-1][0])
            out = query_oracle(aa, bb)
            if len(res) > 0:
                s.append(res[0])
                print("r %s s%i %s" % (r, i, s[i]))
                break
            r+=1
        print("Found s%i: %s" % (i, s[i]))
    
    print("Compute new intervals")
    rmin = ceil((M[i-1][0]*s[i] - B3 + 1)/n)
    rmax = floor((M[i-1][1]*s[i] - B2)/n)
    rs = list(range(rmin, rmax+1))
    intervals = []
    for r in rs:
        newa = max(M[i-1][0], ceil((B2 + r*n)/s[i]))
        newb = min(M[i-1][1], ceil((B3 - 1 + r*n)/s[i]))
        intervals.append(newa)
        intervals.append(newb)
    print(intervals)
    M.append(intervals)

    if newa == newb:
        print('Done')
        break
    else:
        i += 1
