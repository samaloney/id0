from hashlib import sha256

min_pass,max_pass = '', ''
min_val, max_val = 0, 0

with open('rockyou.txt', 'rb') as file:
    for line in file.readlines():
        passwd = line.strip()
        hash = sha256(passwd)
        val = int(hash.hexdigest(), base=16)
        if min_val == 0 and max_val == 0:
            min_val = val
            max_val = val
            min_pass = passwd
            max_pass = passwd
        if val > max_val:
            max_pass = passwd
            max_val = val
        elif val < min_val:
            min_pass = passwd
            min_val = val

print(min_pass, min_val)
print(max_pass, max_val)
print(max_pass+min_pass)           
