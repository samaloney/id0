from string import ascii_uppercase

cipher_text = "ZNKIGKYGXIOVNKXOYGXKGRREURJIOVNKXCNOINOYXKGRRECKGQOSTUZYAXKNUCURJHKIGAYKOSZUURGFEZURUUQGZZNKCOQOVGMKGZZNKSUSKTZHAZOLOMAXKOZYMUZZUHKGZRKGYZROQKLOLZEEKGXYURJUXCNGZKBKXBGPJADLIVBAYKZNUYKRGYZZKTINGXGIZKXYGYZNKYURAZOUT"

def caesar_enc(key, msg):
    offset = ascii_uppercase.index(key)
    cipher = []
    for char in msg:
        index = (ascii_uppercase.index(char) + offset)  % 26
        cipher.append(ascii_uppercase[index])
    return "".join(cipher)

def caesar_dec(key, msg):
    offset = ascii_uppercase.index(key)
    text = []
    for char in msg:
        index = (ascii_uppercase.index(char) - offset)  % 26
        text.append(ascii_uppercase[index])
    return "".join(text)

for i in range(26):
    trial_text = caesar_dec(ascii_uppercase[i], cipher_text)
    ne = trial_text.count('E')
    print(ne, trial_text)
