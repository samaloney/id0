import binascii
import ecdsa
from Crypto.Hash import RIPEMD, SHA256

#secret = 94176137926187438630526725483965175646602324181311814940191841477114099191175

#secret = int('18E14A7B6A307F426A94F8114701E7C8E774E7F9A47E2C2035DB29A206321725', base=16)

#secret = 1


def bitcoin_add(secret):
    g = ecdsa.SECP256k1.generator
    n = g.order()

    pubkey = ecdsa.ecdsa.Public_key(g, g*secret)
    prikey = ecdsa.ecdsa.Private_key(pubkey, secret)
    x = pubkey.point.x()
    y = pubkey.point.y()

    pub = '04' + hex(x)[2:] + hex(y)[2:]

    s1 = SHA256.new()
    s1.update(binascii.unhexlify(pub))

    r1 = RIPEMD.new()
    r1.update(s1.digest())

    cur = '00' + r1.hexdigest()

    d1 = SHA256.new()
    d2 = SHA256.new()
    d1.update(binascii.unhexlify(cur.encode()))
    d2.update(d1.digest())

    checksum = d2.hexdigest()[0:8]

    address = cur + checksum
    code_string = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
    x = int(address, base=16)
    out = []
    while x > 0:
        x, rem = divmod(x, 58)
        out.append(code_string[rem])

    out.append('1')
    out.reverse()
    add = "".join(out)
    return add

test = [[1, '1EHNa6Q4Jz2uvNExL497mE43ikXhwF6kZm'], \
[2, '1LagHJk2FyCV2VzrNHVqg3gYG4TSYwDV4m'], \
[3, '1NZUP3JAc9JkmbvmoTv7nVgZGtyJjirKV1'], \
[4, '1MnyqgrXCmcWJHBYEsAWf7oMyqJAS81eC'], \
[5 , '1E1NUNmYw1G5c3FKNPd435QmDvuNG3auYk']]

for x in test:
    if x[1] != bitcoin_add(x[0]):
        print('Fail')
        break

print(bitcoin_add(94176137926187438630526725483965175646602324181311814940191841477114099191175))
