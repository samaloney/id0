from binascii import unhexlify, hexlify
from Crypto.PublicKey import RSA
import requests


cipher = '912fcd40a901aa4b7b60ec37ce6231bb87783b0bf36f824e51fe77e9580ce1adb5cf894410ff87684969795525a63e069ee962182f3ff876904193e5eb2f34b20cfa37ec7ae0e9391bec3e5aa657246bd80276c373798885e5a986649d27b9e04f1adf8e6218f3c805c341cb38092ab771677221f40b72b19c75ad312b6b95eafe2b2a30efe49eb0a5b19a75d0b31849535b717c41748a6edd921142cfa7efe692c9a776bb4ece811afbd5a1bbd82251b76e76088d91ed78bf328c6b608bbfd8cf1bdf388d4dfa4d4e034a54677a16e16521f7d0213a3500e91d6ad4ac294c7a01995e1128a5ac68bfc26304e13c60a6622c1bb6b54b57c8dcfa7651b81576fc'

with open('pub.key', 'r') as file:
    keystr = file.read()

pub = RSA.importKey(keystr)

t = hexlify('Hello World'.encode())

c2 = pow(2, pub.key.e, pub.key.n)
cmod = int(cipher, base=16) * c2 % pub.key.n

cmod_hex = format(cmod, '0x')

target_url = 'https://id0-rsa.pub/problem/rsa_oracle/'

res = requests.get(target_url+cmod_hex)
print('Hex: ', format(int(res.text, base=16)//2, '0x'))
print('ascii: ',unhexlify(format(int(res.text, base=16)//2, '0x')))
