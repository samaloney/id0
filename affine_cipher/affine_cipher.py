from collections import defaultdict 
from hashlib import md5


def egcd(a, b):
    x,y, u,v = 0,1, 1,0
    while a != 0:
        q, r = b//a, b%a
        m, n = x-u*q, y-v*q
        b,a, x,y, u,v = a,r, u,v, m,n
    gcd = b
    return gcd, x, y


def modinv(a, m):
    gcd, x, y = egcd(a, m)
    if gcd != 1:
        return None  # modular inverse does not exist
    else:
        return x % m


def affine_enc(a, b, alphabet, msg):
    text = []
    m = len(alphabet)
    for char in msg:
        enc = (a * alphabet.index(char) + b) % m
        text.append(alphabet[enc])
    return "".join(text)


def affine_dec(a, b, alphabet, msg):
    text = []
    m = len(alphabet)
    ainv = modinv(a, m)
    for char in msg:
        dec = ainv* (alphabet.index(char) - b) % m
        text.append(alphabet[dec]) 
    return "".join(text)   

codex = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ ,.'

test_text = 'HELLO WORLD.'
test_cipher = 'RLZZC SCIZJB'
res_enc = affine_enc(2, 3, codex, test_text)
if res_enc == test_cipher:
    print('Encryption  test passed')
res_dec = affine_dec(2, 3, codex, test_cipher)
if res_dec == test_text:
    print('Decryption test passed')
    print(md5(res_dec.encode()).hexdigest())

with open('msg', 'r') as file:
    msg = file.read()

d = defaultdict(int)
for char in msg:
    d[char] += 1

dd = sorted(d.items(), key=lambda x: -x[1])

with open('wordfreqs.txt', 'r') as file:
    wf = defaultdict(float)
    for line in file.readlines():
        letter, freq = line.split(' : ')
        wf[letter] = (float(freq.strip())/100.0) * len(msg.strip())

chis = []
ab = []
for a in range(1, len(codex)):
    for b in range(1, len(codex)):
        poss = affine_dec(a, b, codex, msg.strip())
        cf = defaultdict(int)
        for char in poss:
            cf[char] +=1
        chi2 = sum([ ((cf[key] - wf[key])**2)/wf[key] for key in wf])
        ab.append([a,b])  
        chis.append(chi2)

best_ind = chis.index(min(chis))
best_ab = ab[best_ind]

res = affine_dec(best_ab[0], best_ab[1], codex, msg.strip())
resmd5 = md5(res.encode())
print(resmd5.hexdigest())
