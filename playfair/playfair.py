from math import log10, exp
from string import ascii_uppercase
from random import randint, shuffle
import numpy as np


letters = list(ascii_uppercase.replace('J', ''))

def gen_key_square(password, code):
    codex = code[:]
    res = np.empty([25], str)
    last = 0
    for i, char in enumerate(password):
        res[i] = char
        if char in codex:
            codex.remove(char)
        
        last = i
    
    res[last+1:] = codex
    return res.reshape([5,5])


def encode(message, key):
    message = list(message)
    cipher = []
    mlen = len(message)
    
    if mlen % 2 != 0:
        message.append('X')
        mlen +=1

    for x in range(0, mlen - 1, 2):
        if message[x] == message[x+1]:
            message[x+1] = 'X'

        i, j = np.where(key==message[x])
        k, l = np.where(key==message[x+1])

        i, j, k, l = i[0], j[0], k[0], l[0]

        if i != k and j != l:
            cipher.append(key[i,l])
            cipher.append(key[k,j])
        elif i == k:
            cipher.append(key[i, (j+1) % 5])
            cipher.append(key[i, (l+1) % 5])
        elif j ==l:
            cipher.append(key[(i + 1) % 5, j])
            cipher.append(key[(k + 1) % 5, l])
    
    return cipher

def decode(cipher, key):
    cipher = list(cipher)
    clear = []
    mlen = len(cipher)
    for x in range(0, mlen - 1, 2):
        i, j = np.where(key==cipher[x])
        k, l = np.where(key==cipher[x+1])

        i, j, k, l = i[0], j[0], k[0], l[0]

        if i != k and j != l:
            clear.append(key[i,l])
            clear.append(key[k,j])
        elif i == k:
            clear.append(key[i, (j-1) % 5])
            clear.append(key[i, (l-1) % 5])
        elif j ==l:
            clear.append(key[(i-1) % 5, j])
            clear.append(key[(k-1) % 5, l])
   
        if clear[x] == clear[x+1]:
            clear[x+1] = clear[x]
   
    if mlen % 2 == 0 and clear[-1] == 'X':
        return clear[:-1]

    return ''.join(clear)


def modify_key_square(key):
    i = randint(0, 50)
    res = key.copy()
    if i == 0:
        j, k = randint(0,4), randint(0,4)
        tmp = res[j,:].copy()
        res[j,:] = res[k,:]
        res[k,:] = tmp
    elif i == 1:
        j, k = randint(0,4), randint(0,4)
        tmp = res[:,j].copy()
        res[:,j] = res[:,k]
        res[:,k] = tmp
    elif i == 2:
        res = res[::-1]
    elif i == 3:
        res = np.flipud(res)
    elif i == 4:
        res = np.fliplr(res)
    else:
        i,j = divmod(randint(0,24), 5)
        k,l = divmod(randint(0,24), 5)
        tmp = res[i,j]
        res[i,j] = res[k,l]
        res[k,l] = tmp

    return res


def bigrams(msg, size=1):
    d = defaultdict(int)
    for i in range(0, len(msg) - size):
        cur = msg[i:i+size]
        d[cur] += 1
    
    return d


with open('english_quadgrams.txt', 'r') as file:
    ngrams = dict()
    for line in file.readlines():
        ngram, count = line.split(' ')
        ngrams[ngram] = float(count)

    total = sum(ngrams.values())
    for k, v in ngrams.items():
        ngrams[k] = log10(v/total)
    minp = log10(0.01/total)


def score(text, ngrams):
    score = 0
    size = len(next (iter (ngrams.keys())))
    for i in range(len(text) - size):
        if text[i:i+size] in ngrams: score += ngrams[text[i:i+size]]
        else: score += minp
    
    return score


with open('cipher.txt', 'r') as file:
    ctext = file.read()

def crack():
    shuffle(letters)
    parent_key = np.array(letters)
    parent_key = parent_key.reshape([5,5])
    parent_score = score(decode(ctext, parent_key), ngrams)
    max_score = parent_score
    max_key = parent_key

    iterations = 15000
    temp = 100
    step = 1.0
    for t in np.arange(temp, -0.1, -step):
        for i in range(iterations):
            child_key = modify_key_square(parent_key)
            child_score = score(decode(ctext, child_key), ngrams)
            dF = child_score - parent_score
            if dF >= 0:
                parent_key = child_key
                parent_score = child_score
            elif t > 0:
                p = exp(dF/t)
                if p > randint(0,100)/100.0:
                    parent_key = child_key
                    parent_score = child_score

            if parent_score > max_score:
                max_score = parent_score
                max_key = parent_key

        #print("T: %s\nMax Score: %s\nKey: %s" % (t, max_score, max_key))
    return (max_score, max_key)

out = crack()
msg = decode(ctext, out[1])
print(msg)
