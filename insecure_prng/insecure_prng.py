from binascii import unhexlify
from hashlib import sha256


m = 2 ** 31
a = 1103515245
c = 12345
seed = 0x123

def lcg(x):
    return (a*x +c) % m


def bits_to_bytes(bits):
    bytevals = []
    for i in range(len(bits), 0, -8):
        curbits = bits[i-8:i]
        curbits.reverse()
        val = sum([ curbits[x]*2**x for x in range(8) ])
        bytevals.append(format(val, '02x'))
    
    bytevals.reverse()
    return ''.join(bytevals)


def gen_key(x):
    bits = []
    last = x
    for i in range(256):
        next = lcg(last)
        bit = next >> 29 & 1
        bits.append(bit)
        last = next

    return bits


def base58_encode(val):
    code_string = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
    encoded = []
    while val > 0:
        val, rem = divmod(val, 58)
        encoded.append(code_string[rem])
       
    encoded.reverse()
    return ''.join(encoded)


def base58_decode(val):
    decoded = 0
    multi = 1
    code_string = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
    for char in val[::-1]:
        decoded += multi * code_string.index(char)
        multi *= 58
    
    return decoded


def wif_encode(val):
    val = '80' + val
    hash1 = sha256(unhexlify(val))
    hash2 = sha256(hash1.digest())
    val = val + hash2.hexdigest()[0:8]
    x = int(val, base=16)
    return base58_encode(x) 


def wif_decode(wif):
    num = base58_decode(wif)
    hstring = format(num, '02x')
    return hstring[2:len(hstring) - 8]


print(seed)
test_wif = wif_encode('0' + format(seed, '02x'))
print(test_wif)
hex1 = wif_decode(test_wif)
print(hex1)
bits2 = gen_key(int(hex1, base=16)) 
hex2 = bits_to_bytes(bits2)
print(hex2)
wif2 = wif_encode(hex2)
print(wif2)

wif1 = '5KQFVHAxyMMVsDz75bDp7S4NpwoQz2FgR8b7DjyEhUo6saJfS73'
hex1 = wif_decode(wif1)
bits2 = gen_key(int(hex1, base=16))
hex2 = bits_to_bytes(bits2)
wif2 = wif_encode(hex2)
print(wif2)



