//
//  main.cpp
//  test
//
//  Created by Shane Maloney on 22/01/2016.
//  Copyright © 2016 Shane Maloney. All rights reserved.
//

#include <bitset>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>

std::string const hex_known = "d0d0b841bafcca097cf744ed94b90daced8ce586085a2e86f0eecc521f9aea9d";
//std::string const hex_known = "3a71c3dc3b5dad959973a074cff234bf09735ed305dfc6247357142a962bd3fa";
int lcg(int prev) {
    return ((prev * 1103515245) + 12345) & 0x7fffffff;
}

int test_key(int seed) {
    std::bitset<8> key [32];
    std::stringstream hexval;
    div_t divresult;
    int prev = seed;
    int current = 0;
    for (int i=0; i<256; i++) {
        divresult = div(i, 8);
        current = lcg(prev);
        key[divresult.quot][7-divresult.rem] = current >> 29 & 1;
        prev = current;
    }
    for (int i = 0; i<32; i++) {
        hexval << std::setfill('0') << std::setw(2) << std::hex << key[i].to_ullong();
    }
    std::string hex_test = hexval.str();
    if (hex_test == hex_known) {
        std::cout << "Done";
    } else {
        current = -1;
    }
    return current;
}

int main(int argc, const char * argv[]) {
    for (int i = 0; i < 2147483647; i++) {
        if (i % 100000 == 0) {
            std::cout << i << '\n';
        }
        int cur = test_key(i);
        if (cur > 0) {
            std::cout << cur;
        }
    }
    exit(0);
}
