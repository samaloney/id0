#!/usr/bin/env bash

openssl rsautl -decrypt -in <(xxd -r -p ciphertxt) -inkey prikey -raw | xxd -p -c 8 | tail -n1
