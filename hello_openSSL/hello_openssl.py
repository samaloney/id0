from Crypto.PublicKey import RSA

msg = "6794893f3c47247262e95fbed846e1a623fc67b1dd96e13c7f9fc3b880642e42"
test = "71b71b4f55ba6c921c33fbe91f51bf888bba9761134c2ac6456e02f3802b1b69"

with open('prikey', 'r') as file:
    key_string = file.read()

key = RSA.importKey(key_string)

print(hex(key.decrypt(int(test, base=16))))
print(hex(key.decrypt(int(msg, base=16))))
