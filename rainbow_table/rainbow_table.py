from hashlib import sha256
from binascii import hexlify

with open('rockyou.txt', 'rb') as file:
    passwords = file.readlines()
    plen = len(passwords)


def passhash(pw):
    val = ''.encode()
    for i in range(50000):
        val = sha256(val+pw).digest()
    return hexlify(val)

def hashrev(pwhash, col):
    num = int(pwhash, base=16)
    line = (num + col) % plen
    return passwords[line]

target = '27ce84a6075b583086d9fc0c856f1da5d9a853507faffd7d70833c1b7accb156'
start = 'bambino'.encode()
end = 'hunter42'
cur = start
out = []

for i in range(200):
    out.append(cur)
    hs = passhash(cur)
    pw = hashrev(hs, i)[0:-1]
    out.append(hs)
    cur = pw

ind = out.index(target.encode())
print(out[ind-1])
    
    
