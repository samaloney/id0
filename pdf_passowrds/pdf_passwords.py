from Crypto.Cipher import ARC4
from hashlib import md5
from binascii import unhexlify


user_hash =  unhexlify(b'fe97c8762da0f0201eb463df6e181288')
key_length = 128 // 8 #bits to bytes
revision = 3
permissions = unhexlify(b'fcffffff')
owner_hash = unhexlify(b'79e64c5fd1d84a4ae2bc07e82636b34b5969c4fe6bcce36886db40dece66f0e2')
document_id = unhexlify(b'bfb6e1530b109ae5d10cf52562e8bd3a')
padding_string = unhexlify(b'28BF4E5E4E758A4164004E56FFFA01082E2E00B6D0683E802F0CA9FE6453697A')
padding_len = len(padding_string)


def gen_symmetric_key(password):
    n = len(password)
    if n < 32:
        password = password + padding_string
    password = password[:32]
    val = password + owner_hash + permissions + document_id
    hashf = md5(val).digest()
    for i in range(50):
        hashf = md5(hashf).digest()

    return hashf[:key_length]


def gen_user_hash(key):
    val = padding_string + document_id
    hf = md5(val)
    cipher = ARC4.new(key)
    ctext = cipher.encrypt(hf.digest())
    
    for i in range(1, 20):
        new_key = bytearray(key_length)
        for j in range(key_length):
            new_key[j] = key[j] ^ i

        cipher = ARC4.new(bytes(new_key))
        ctext = cipher.encrypt(ctext)

    return ctext


with open('rockyou.txt', 'rb') as file:
    passwords = file.readlines()

for password in passwords:
    password = password.strip()

    sys_key = gen_symmetric_key(password)
    trail_hash = gen_user_hash(sys_key)
    if trail_hash == user_hash:
        print(password)
        break

