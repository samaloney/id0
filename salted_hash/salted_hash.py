from hashlib import sha256
import base64


def salted_hash(salt, password):
    sha_hash = sha256(password+salt.encode())
    return '$'.encode() + salt.encode() + '$'.encode() + base64.b64encode(sha_hash.digest())


with open('hashes.txt', 'r') as file:
    hashes = [x.strip() for x in file.readlines()]

with open('rockyou.txt', 'rb') as file:
        passwds = file.readlines()

known = []

for passwd in passwds:
    for hs in hashes:
        gar, salt, hashed = hs.split('$')
        res = salted_hash(salt, passwd.strip())
        if res == hs.encode():
            known.append(passwd.strip())

passes = [ x.decode() for x in known]
passes.sort()
print(''.join(passes))  
