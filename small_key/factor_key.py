from Crypto.PublicKey import RSA
from math import floor, gcd, sqrt
import pfact

def egcd(a, b):
    x,y, u,v = 0,1, 1,0
    while a != 0:
        q, r = b//a, b%a
        m, n = x-u*q, y-v*q
        b,a, x,y, u,v = a,r, u,v, m,n
    gcd = b
    return gcd, x, y


def modinv(a, m):
    gcd, x, y = egcd(a, m)
    if gcd != 1:
        return None  # modular inverse does not exist
    else:
        return x % m

def pfactor(n):
        step = lambda x: 1 + (x<<2) - ((x>>1)<<1)
        maxq = int(floor(sqrt(n)))
        d = 1
        q = n % 2 == 0 and 2 or 3
        while q <= maxq and n % q != 0:
                q = step(d)
                d += 1
        return q <= maxq and [q] + pfactor(n//q) or [n]

with open('pub.key', 'r') as file:
    keystr = file.read()

pub = RSA.importKey(keystr)

#p, q = pfact.pfactor(pub.key.n)
#Python code/cython code wasn't working so used sage, should look at again

p = 878291059745115859
q = 662700133751480051

d = modinv(pub.key.e, (p-1)*(q-1))
print(hex(d))

