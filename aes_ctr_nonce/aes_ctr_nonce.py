from binascii import hexlify, unhexlify

def strxor(a, b):
    al = len(a)
    bl = len(b)
    if al > bl:
        return "".join([format((x^y), '02x') for (x, y) in zip(unhexlify(a[:bl]), unhexlify(b))])
    else:
        return "".join([format((x^y), '02x') for (x, y) in zip(unhexlify(a), unhexlify(b[:al]))])


c1 = '369f9e696bffa098d2bb383fb148bd90'
c2 = '23d7847f28e4b6cc86be386cb64ca281'
#c1 = '3b101c091d53320c000910'
#c2 = '071d154502010a04000419'

cxor = strxor(c1, c2)

def cribdrag(message, crib):
    res = {}
    guess = "".join([ format(ord(x), '02x') for x in crib])
    for i in range(len(message)-len(guess)+1):
        z = message[i:i+len(guess)]
        text = unhexlify(strxor(z, guess))
        if text.decode('ascii', 'ignore').replace(' ', '').isalpha():
            res[i] = text
        #print('[%d] -> %s' % (i, text))
    return res

# Start of with crib of ' the ' find good text of 't mes' -> 't message'
# Crib of 't message' find good text of ' the text'. Find all wording ending in t up to 8 chars long
# grep -e '^.\{1,7\}t$' /usr/share/dict/web2a | wc -l use each as reset of messesge and look a output
# "secret message" get 'is is the text' -> 'this is the text' -> 'a secret message'
