from binascii import unhexlify
from hashlib import md5
from Crypto.Cipher import AES
from datetime import datetime, timedelta

start = datetime.strptime('25-01-16', '%d-%m-%y')
end = start + timedelta(days=7)

ctext = unhexlify('a99210d796a1e37503febf65c329c1b2')
ptext = ''

g = []

for i in range(int(start.timestamp()), int(end.timestamp())):
    time_string = str(i).encode('ascii')
    key = md5(time_string).digest()
    cipher = AES.new(key)
    res = cipher.decrypt(ctext)
    try:
        text = res.decode('ascii')
        g.append(text)
    except Exception as e:
        pass

print(g)
