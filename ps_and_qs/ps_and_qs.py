from Crypto.PublicKey import RSA
from math import gcd


def egcd(a, b):
    x,y, u,v = 0,1, 1,0
    while a != 0:
        q, r = b//a, b%a
        m, n = x-u*q, y-v*q
        b,a, x,y, u,v = a,r, u,v, m,n
    gcd = b
    return gcd, x, y


def modinv(a, m):
    gcd, x, y = egcd(a, m)
    if gcd != 1:
        return None  # modular inverse does not exist
    else:
        return x % m

ctext = '0xf5ed9da29d8d260f22657e091f34eb930bc42f26f1e023f863ba13bee39071d1ea988ca62b9ad59d4f234fa7d682e22ce3194bbe5b801df3bd976db06b944da'

c = int(ctext, base=16)

with open('key1', 'r') as k1, open('key2', 'r') as k2:
    k1_str = k1.read()
    k2_str = k2.read()

key1 = RSA.importKey(k1_str)
key2 = RSA.importKey(k2_str)

p = gcd(key1.key.n, key2.key.n)

q1 = key1.key.n // p
q2 = key2.key.n // p

d1 = modinv(key1.key.e, (p-1)*(q1-1))
msg = hex(pow(c, d1, key1.key.n))
print(msg) 

