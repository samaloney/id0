

def egcd(a, b):
    x,y, u,v = 0,1, 1,0
    while a != 0:
        q, r = b//a, b%a
        m, n = x-u*q, y-v*q
        b,a, x,y, u,v = a,r, u,v, m,n
    gcd = b
    return gcd, x, y


def modinv(a, m):
    gcd, x, y = egcd(a, m)
    if gcd != 1:
        return None  # modular inverse does not exist
    else:
        return x % m


def add(p, q, n):
    lamdiv = modinv((q[0] - p[0]) % n, n)
    lam = ((q[1] -  p[1]) * lamdiv) % n
    rx = ((lam ** 2) -p[0] - q[0]) % n
    ry = (lam * (p[0] - rx) -p[1]) % n
    return (rx, ry)


def double(p, n):
    lamdiv = modinv( 2*p[1], n)
    lam = (3 * (p[0] ** 2) + a) * lamdiv % n
    rx = ((lam ** 2) - (2*p[0])) % n
    ry = (lam * (p[0] - rx) - p[1]) % n
    return (rx, ry)


p =  0xffffffff00000001000000000000000000000000ffffffffffffffffffffffff
a = -0x3
b =  0x5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b
G = (0x6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296,
             0x4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5)
Q = (0x52910a011565810be90d03a299cb55851bab33236b7459b21db82b9f5c1874fe,
             0xe3d03339f660528d511c2b1865bcdfd105490ffc4c597233dd2b2504ca42a562)

d = 2
cur = double(G, p)
while True:
    
    if cur == Q:
        break
    else:
        cur = add(G, cur, p)
    
    d+=1


print(d)
