from binascii import unhexlify
import requests

c = 'c6574d8a54c952a7f298673ee7063c16ecf5f6d6405e2ad74254ff211635e390'

server = 'http://id0-rsa.pub/problem/cbc_padding_oracle/'

r = requests.Session()

ivorig = c[0:32]
c1 = c[32:]
ivcur = list('0'*32)
inter = ivcur[:]
pguess = ivcur[:]
pknown = ivcur[:]

ps = 1 
for i in range(len(ivcur)-2, -1, -2):
    for j in range(0,256):
        ivcur[i:i+2] = format(j, '02x')
        cp = ''.join(ivcur) + c1
        print(i, j, cp)
        res = r.get(server+cp)
        if res.status_code != 500:
            inter[i:i+2] = format(j ^ ps, '02x')
            pguess[i:i+2] = format(ps, '02x')
            pknown[i:i+2] = format(int(''.join(inter[i:i+2]), base=16) ^ int(ivorig[i:i+2], base=16), '02x')
            print('IV*        : %s \nPlain Guess: %s \nInt. Val.  : %s \nIV Orig    : %s \nPlain Text : %s' %\
                    (''.join(ivcur), ''.join(pguess), ''.join(inter), ivorig, ''.join(pknown))) 
            ps += 1
            for k in range(i,len(ivcur),2):
                ivcur[k:k+2] = format(int(''.join(inter[k:k+2]), base=16) ^ ps, '02x')
            print("Next bit")
            print('IV*        : %s \nPlain Guess: %s \nInt. Val.  : %s \nIV Orig    : %s \nPlain Text : %s' %\
                                        (''.join(ivcur), ''.join(pguess), ''.join(inter), ivorig, ''.join(pknown)))
            break
    if j == 255:
        print("Nooo:")
        break

print(unhexlify(''.join(pknown).encode()))
