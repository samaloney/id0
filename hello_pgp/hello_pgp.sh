#!/usr/bin/env bash

#< /usr/share/dict/words xargs -I -P16 -n 1 gpg2 --batch --passphrase {} -d msg 2>/dev/nul
# passphrase "seamanships",message "passionately apathetic"

main() {
    i=0
    for word in $(cat /usr/share/dict/web2);
    do
        msg=$(gpg2 --batch --passphrase "$word" -d msg 2> /dev/null)
        if [ $? -eq 0 ]; then
            echo "$msg"
            exit 0
    	fi
	if [ $(( $i % 1000 )) -eq 0 ]; then
	    echo "$word"
	fi
	i=$i+1
    done
    exit -1
}
main
