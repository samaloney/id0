import requests
from string import ascii_lowercase as letters
from random import shuffle
server = 'https://id0-rsa.pub/problem/crime-oracle/'
session = requests.Session()

letters = list(letters)

res = []
for i in range(8):
    for char in letters:
        padding = letters[:]
        padding.pop(padding.index(char))
        #shuffle(padding)
        pad = ''.join(padding)
        url = 'cookie=' + ''.join(res[:i]) + char + pad[0:13]
        resp = session.get(server+url)
        size = int(resp.text)
        print(i, char, url)
        if size < 112:
            res.append(char)
            last = 0
            print("Found: ", res)
            break

print(''.join(res))
